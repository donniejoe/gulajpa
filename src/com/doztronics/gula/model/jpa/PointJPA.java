package com.doztronics.gula.model.jpa;

import java.io.Serializable;
import java.lang.Long;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: PointJPA
 *
 */
@Entity

public class PointJPA implements Serializable {

	   
	@Id @GeneratedValue
	private Long id;
	private int x;
	private int y;
	private static final long serialVersionUID = 1L;

	public PointJPA() {
		super();
	}   
	public PointJPA(int x, int y) {
		super();
		this.x = x;
		this.y = y;
	}   
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}   
	public int getX() {
		return this.x;
	}

	public void setX(int x) {
		this.x = x;
	}   
	public int getY() {
		return this.y;
	}

	public void setY(int y) {
		this.y = y;
	}
   
	@Override
    public String toString() {
        return String.format("(%d, %d)", this.x, this.y);
    }
}
