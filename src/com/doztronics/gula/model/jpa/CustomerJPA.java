package com.doztronics.gula.model.jpa;

import java.io.Serializable;
import java.lang.String;
import java.sql.Date;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: CustomerJPA
 *
 */
@Entity

public class CustomerJPA implements Serializable {

	   
	@Id
	private String id;
	private String name;
	private Date dateOfBirth;
	private static final long serialVersionUID = 1L;

	public CustomerJPA() {
		super();
	}   
	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}   
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}   
	public Date getDateOfBirth() {
		return this.dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
   
	@Override
	public String toString() {
		return this.getClass().getName() + ", " + "id" + " : " + id + ", name : " + name; 
	}
}
