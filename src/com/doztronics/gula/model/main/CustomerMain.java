package com.doztronics.gula.model.main;

import javax.persistence.*;

import com.doztronics.gula.model.jpa.CustomerJPA;

import java.util.*;


public class CustomerMain {
	
	public static void main(String[] args) {
        // Open a database connection
        // (create a new database if it doesn't exist yet):
        EntityManagerFactory emf =
            Persistence.createEntityManagerFactory(CustomerJPA.class.getName());
        EntityManager em = emf.createEntityManager();

        // Store 1000 Point objects in the database:
        em.getTransaction().begin();
        for (int i = 0; i < 1000; i++) {
            CustomerJPA p = new CustomerJPA();
            em.persist(p);
        }
        em.getTransaction().commit();

        // Find the number of Point objects in the database:
        Query q1 = em.createQuery("SELECT COUNT(p) FROM Point p");
        System.out.println("Total Points: " + q1.getSingleResult());

        // Find the average X value:
        Query q2 = em.createQuery("SELECT AVG(p.x) FROM Point p");
        System.out.println("Average X: " + q2.getSingleResult());

        // Retrieve all the Point objects from the database:
        TypedQuery<CustomerJPA> query =
            em.createQuery("SELECT id FROM Point p", CustomerJPA.class);
        List<CustomerJPA> results = query.getResultList();
        for (CustomerJPA p : results) {
            System.out.println(p);
        }

        // Close the database connection:
        em.close();
        emf.close();
    }

}
