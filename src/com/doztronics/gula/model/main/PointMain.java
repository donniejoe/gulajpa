package com.doztronics.gula.model.main;

import javax.persistence.*;

import com.doztronics.gula.model.jpa.PointJPA;

import java.util.*;


public class PointMain {

	public static void main(String[] args) {
        // Open a database connection
        // (create a new database if it doesn't exist yet):
        EntityManagerFactory emf =
            Persistence.createEntityManagerFactory("GulaJPA");
        EntityManager em = emf.createEntityManager();

        // Store 1000 PointJPA objects in the database:
        em.getTransaction().begin();
        for (int i = 0; i < 1000; i++) {
            PointJPA p = new PointJPA(i, i);
            em.persist(p);
        }
        em.getTransaction().commit();

        // Find the number of PointJPA objects in the database:
        Query q1 = em.createQuery("SELECT COUNT(p) FROM PointJPA p");
        System.out.println("Total Points: " + q1.getSingleResult());

        // Find the average X value:
        Query q2 = em.createQuery("SELECT AVG(p.x) FROM PointJPA p");
        System.out.println("Average X: " + q2.getSingleResult());

        // Retrieve all the PointJPA objects from the database:
        TypedQuery<PointJPA> query =
            em.createQuery("SELECT p FROM PointJPA p", PointJPA.class);
        List<PointJPA> results = query.getResultList();
        for (PointJPA p : results) {
            System.out.println(p);
        }

        // Close the database connection:
        em.close();
        emf.close();

	}

}
